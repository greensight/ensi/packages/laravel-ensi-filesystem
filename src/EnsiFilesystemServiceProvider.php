<?php

namespace Ensi\LaravelEnsiFilesystem;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class EnsiFilesystemServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/ensi-filesystem.php', 'ensi-filesystem');

        $this->app->scoped('ensi.filesystem', function (Application $app) {
            return new EnsiFilesystemManager($app);
        });
        $this->app->alias('ensi.filesystem', EnsiFilesystemManager::class);
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/ensi-filesystem.php' => config_path('ensi-filesystem.php'),
            ], 'config');
        }
    }
}
