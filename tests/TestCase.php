<?php

namespace Ensi\LaravelEnsiFilesystem\Tests;

use Ensi\LaravelEnsiFilesystem\EnsiFilesystemServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function getPackageProviders($app): array
    {
        return [
            EnsiFilesystemServiceProvider::class,
        ];
    }
}
